# Nill-Ratios-of-Number-Bases

Works out nill ratios in different number bases.

This program has a function to work out the nill ratio of integers >= 0 in different number bases.

The nill ratio of a number in a base is the number of trailing zeros in the representation divided by the number of digits. 

This can be used to compare efficency of traling zeros in different number bases.

The default behavior compares the mean nill ratio of base 12 to base 2 for ints 1 to 136039399. 

This software is licensed under the CC0 licence, which means it is in the public domain.