import math

def traling_zero_ratio(number, base): # returns the ratio of how many traling zeroes to the length of the number
    if base == 2:
        bin_num = bin(number)[2:] # binary representation with no 0b
        tzeroes = 0
        for bit in reversed(bin_num): # reversed so it goes right to left
            if bit != "0":
                break
            tzeroes = tzeroes + 1

        return tzeroes/len(bin_num)

    else:
        tzeroes = 0
        total_digits = math.floor(math.log(number, base)) + 1 # the number of digits required to represent the number
        while True: 
            number, remainder = divmod(number, base) # works out each digit, works out right numbers first, so
                                                     # as soon as it finds a non-zero number, it is the end of trailing zeroes

            if remainder == 0:
                tzeroes = tzeroes+1
            else:
                break # found all traling zeroes

            if number == 0: # no more digits
                break

        return tzeroes/total_digits


            

if __name__ == "__main__": # if not imported
    
    upperbound = 136039399 # a large prime number to check until, can increase to get a more accurate average



    # working out mean traling zeroes of binary and dozenal

    bin_total = 0
    doz_total = 0

    for n in range(1, upperbound+1):
        
        bin_total = bin_total + traling_zero_ratio(n, 2)
        doz_total = doz_total + traling_zero_ratio(n, 12)
        if n % 10000000 == 0: # prints current mean every so often
            print("Binary", bin_total/n)
            print("Dozenal", doz_total/n)


    print("Final") # final results

    print("Binary", bin_total/n)
    print("Dozenal", doz_total/n)
